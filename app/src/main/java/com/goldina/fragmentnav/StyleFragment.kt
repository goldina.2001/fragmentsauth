package com.goldina.fragmentnav

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RadioGroup
import android.widget.TextView
import androidx.fragment.app.Fragment

class StyleFragment : Fragment() {
    private lateinit var radioGroupBckgrnd: RadioGroup
    private lateinit var radioGroupFont: RadioGroup
    private lateinit var radioGroupSize: RadioGroup
    private lateinit var button: Button
    private lateinit var textView: TextView
    private var bckgrnd:Int=0
    private var font:Int=0
    private var size:Int=0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_style, container, false)
        radioGroupBckgrnd=view.findViewById(R.id.radioGroupBckrgnd)
        radioGroupFont=view.findViewById(R.id.radioGroupFont)
        radioGroupSize=view.findViewById(R.id.radioGroupSize)
        textView = activity?.findViewById(R.id.textViewCheck)!!

        button = view.findViewById(R.id.button)
        radioGroupBckgrnd.setOnCheckedChangeListener { group, checkedId ->
            bckgrnd=checkedId
        }
        radioGroupFont.setOnCheckedChangeListener { group, checkedId ->
            font=checkedId
        }
        radioGroupSize.setOnCheckedChangeListener { group, checkedId ->
            size=checkedId
        }
        button.setOnClickListener {
            val checkList=listOf(bckgrnd,font,size)
             when(checkList){
                listOf(R.id.radioButtonGreen,R.id.radioButton1,R.id.radioButtonMin)-> textView.text = R.style.tv1.toString()
                listOf(R.id.radioButtonGreen,R.id.radioButton2,R.id.radioButtonMin)-> textView.text = R.style.tv2.toString()
                listOf(R.id.radioButtonBlue,R.id.radioButton1,R.id.radioButtonMin)-> textView.text = R.style.tv3.toString()
                listOf(R.id.radioButtonBlue,R.id.radioButton2,R.id.radioButtonMin)-> textView.text = R.style.tv4.toString()
                listOf(R.id.radioButtonGreen,R.id.radioButton1,R.id.radioButtonMedium)-> textView.text = R.style.tv5.toString()
                listOf(R.id.radioButtonGreen,R.id.radioButton2,R.id.radioButtonMedium)-> textView.text = R.style.tv6.toString()
                listOf(R.id.radioButtonBlue,R.id.radioButton1,R.id.radioButtonMedium)-> textView.text = R.style.tv7.toString()
                listOf(R.id.radioButtonBlue,R.id.radioButton2,R.id.radioButtonMedium)-> textView.text = R.style.tv8.toString()
            }
        }
        return view
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            StyleFragment()
    }
}