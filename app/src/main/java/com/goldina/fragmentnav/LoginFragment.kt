package com.goldina.fragmentnav

import android.content.ClipData
import android.os.Bundle
import android.text.TextUtils
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import com.goldina.fragmentnav.databinding.ActivityMainBinding.inflate
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class LoginFragment : Fragment() {
    private lateinit var auth: FirebaseAuth
    private lateinit var db: FirebaseDatabase
    private lateinit var users: DatabaseReference

    private lateinit var bottom_nav: BottomNavigationView

    private lateinit var itemAccount:MenuItem
    private lateinit var buttonHome:Button
    private lateinit var buttonRgstr:Button
    private lateinit var buttonSign:Button
    private lateinit var editTextEmail: EditText
    private lateinit var editTextPass: EditText


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_login,container,false)

        buttonHome=view.findViewById(R.id.buttonHome)
        buttonRgstr=view.findViewById(R.id.buttonRgstr)
        buttonSign=view.findViewById(R.id.buttonSign)

        bottom_nav = activity?.findViewById(R.id.bottom_nav)!!

        auth = FirebaseAuth.getInstance()
        db = FirebaseDatabase.getInstance()
        users= db.getReference("Users")

        editTextEmail=view.findViewById(R.id.username)
        editTextPass=view.findViewById(R.id.password)

        buttonHome.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_homeFragment)
        }
        buttonRgstr.setOnClickListener {
           findNavController().navigate(R.id.action_loginFragment_to_registrationFragment)
        }
        buttonSign.setOnClickListener {
            if(TextUtils.isEmpty(editTextEmail.text.toString())) {
                Toast.makeText(activity, "Введите email", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if(editTextPass.text.toString().length <5) {
                Toast.makeText(activity, "Введите пароль", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            auth.signInWithEmailAndPassword(editTextEmail.text.toString(),editTextPass.text.toString())
                .addOnSuccessListener {
                    bottom_nav.menu.findItem(R.id.account).isVisible=true
                    findNavController().navigate(R.id.action_loginFragment_to_homeFragment)
                }.addOnFailureListener{
                    Toast.makeText(activity, "Ошибка: ${it.message}", Toast.LENGTH_SHORT).show()
                }

        }

        return view
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            LoginFragment()

    }
}