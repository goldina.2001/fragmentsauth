package com.goldina.fragmentnav

import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

class AccountFragment : Fragment() {
    private lateinit var auth: FirebaseAuth
    private lateinit var users: DatabaseReference
    private lateinit var db: FirebaseDatabase

    private lateinit var bottom_nav: BottomNavigationView

    private lateinit var textViewLastName: TextView
    private lateinit var textViewName: TextView
    private lateinit var textViewEmail: TextView
    private lateinit var buttonExit: Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_account, container, false)
        textViewLastName=view.findViewById(R.id.textViewLastName)
        textViewName=view.findViewById(R.id.textViewName)
        textViewEmail=view.findViewById(R.id.textViewEmail)

        buttonExit = view.findViewById(R.id.buttonExit)
        bottom_nav = activity?.findViewById(R.id.bottom_nav)!!

        auth = FirebaseAuth.getInstance()
        db = FirebaseDatabase.getInstance()
        users= db.getReference("Users")
            users.child(FirebaseAuth.getInstance().currentUser!!.uid).addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot){
                    textViewLastName.text = dataSnapshot.child("lastName").value.toString()
                    textViewName.text = dataSnapshot.child("name").value.toString()
                    textViewEmail.text = dataSnapshot.child("email").value.toString()
                }
                override fun onCancelled(error: DatabaseError) {
                    // Failed to read value
                    Log.w(TAG, "Failed to read value.", error.toException())
                }
        })
        buttonExit.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            bottom_nav.menu.findItem(R.id.account).isVisible=false
            activity?.onBackPressed()
        }
        return view
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            AccountFragment()
    }
}