package com.goldina.fragmentnav

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.goldina.fragmentnav.databinding.StringItemBinding

class MyStringAdapter:RecyclerView.Adapter<MyStringAdapter.MyStringHolder>() {
    val stringList = ArrayList<MyString>()
    class MyStringHolder(item:View):RecyclerView.ViewHolder(item) {
        val binding = StringItemBinding.bind(item)
        @RequiresApi(Build.VERSION_CODES.M)
        fun bind(myString: MyString)=with(binding){
            textViewString.text=myString.text
            textViewString.setTextAppearance(myString.style)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyStringHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.string_item,parent,false)
        return MyStringHolder(view)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: MyStringHolder, position: Int) {
        holder.bind(stringList[position])
    }

    override fun getItemCount(): Int {
        return stringList.size
    }

    fun addString(myString: MyString){
        stringList.add(myString)
        notifyDataSetChanged()
    }
}