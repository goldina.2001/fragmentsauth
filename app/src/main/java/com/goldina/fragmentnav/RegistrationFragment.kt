package com.goldina.fragmentnav

import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.goldina.fragmentnav.models.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class RegistrationFragment : Fragment() {
    private lateinit var auth: FirebaseAuth
    private lateinit var db: FirebaseDatabase
    private lateinit var users: DatabaseReference
    private lateinit var buttonLogin:Button
    private lateinit var buttonRegister:Button
    private lateinit var editTextLastName: EditText
    private lateinit var editTextName: EditText
    private lateinit var editTextEmail: EditText
    private lateinit var editTextPass: EditText

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_registration, container, false)

        buttonLogin=view.findViewById(R.id.buttonLogin)
        buttonRegister=view.findViewById(R.id.buttonSign)

        auth = FirebaseAuth.getInstance()
        db = FirebaseDatabase.getInstance()
        users= db.getReference("Users")

        editTextLastName=view.findViewById(R.id.lastname)
        editTextName=view.findViewById(R.id.name)
        editTextEmail=view.findViewById(R.id.username)
        editTextPass=view.findViewById(R.id.password)

        buttonLogin.setOnClickListener {
            findNavController().navigate(R.id.action_registrationFragment_to_loginFragment)
        }
        buttonRegister.setOnClickListener {
           if(TextUtils.isEmpty(editTextLastName.text.toString())) {
                Toast.makeText(activity, "Введите фамилию", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if(TextUtils.isEmpty(editTextName.text.toString())) {
                Toast.makeText(activity, "Введите имя", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if(TextUtils.isEmpty(editTextEmail.text.toString())) {
                Toast.makeText(activity, "Введите email", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if(editTextPass.text.toString().length <5) {
                Toast.makeText(activity, "Пароль должен состоять из более 5 символов", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            auth.createUserWithEmailAndPassword(editTextEmail.text.toString(),editTextPass.text.toString())
                .addOnSuccessListener {
                    var user = User(editTextName.text.toString()
                        ,editTextLastName.text.toString()
                        ,editTextEmail.text.toString()
                        ,editTextPass.text.toString())
                    users.child(FirebaseAuth.getInstance().currentUser!!.uid).setValue(user).addOnSuccessListener {
                        Toast.makeText(activity, "Регистрация завершена", Toast.LENGTH_LONG).show()
                    }.addOnFailureListener{
                        Toast.makeText(activity, "Ошибка: ${it.message}", Toast.LENGTH_LONG).show()
                    }
                }

        }
        return view
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            RegistrationFragment()

    }
}